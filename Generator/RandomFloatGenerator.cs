using System;

namespace Game.Generator
{
    public class RandomFloatGenerator: RandomGenerator
    {
        public float GenerateFloatNumber()
        {
            var buffer = new byte[4];
            new Random().NextBytes(buffer);
            return BitConverter.ToSingle(buffer,0);
        }
    }
}