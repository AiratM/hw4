using System;

namespace Game.Generator
{
    public class RandomGenerator : IRandomGenerator
    {
        public int GenerateNumberInRange(int minValue, int maxValue)
        {
            return new Random().Next(minValue, maxValue);
        }
    }
}