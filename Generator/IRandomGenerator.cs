namespace Game.Generator
{
    public interface IRandomGenerator
    {

        ///<summary> Случайное число в заданном диапазоне <summary>
        int GenerateNumberInRange(int minValue, int maxValue);
    }
}
