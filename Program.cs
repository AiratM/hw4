﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using Game.Configurator;
using Game.Generator;
using Microsoft.Extensions.DependencyInjection;
namespace Game
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var services = new ServiceCollection()
            .AddScoped<IConfiguratorReader, ConfiguratorXMLFileReader>()
            .AddScoped<IConfiguratorWriter, ConfiguratorXMLFileWriter>()
            .AddScoped<IConfigurationValidator, ConfigurationValidator>()
            .AddScoped<IConfigurator, GameConfigurator>()
            .AddScoped<IRandomGenerator, RandomGenerator>()
            .AddScoped<IGame, RandomGame>();

            var serviceProvider = services.BuildServiceProvider();
            var randomGame = serviceProvider.GetService<IGame>() ?? throw new NullReferenceException(nameof(IGame));
            await randomGame.RunGameAsync();
            Console.WriteLine("finished");
            Console.ReadKey();
            
        }

    }
}