using System;
using System.Threading.Tasks;
using Game.Configurator;
using Game.Generator;

namespace Game
{
    public class RandomGame : IGame
    {
        private IConfigurator _gameConfigurator;
        private IRandomGenerator _randomGenerator;

        private GameConfigurationType _gameConfig;

        public async Task RunGameAsync()
        {
            await Task.Run(() => NewRandomGame());
        }

        public RandomGame(IConfigurator gameConfigurator, IRandomGenerator randomGenerator)
        {
            _gameConfigurator = gameConfigurator ?? throw new NullReferenceException(nameof(IConfigurator));
            _randomGenerator = randomGenerator ?? throw new NullReferenceException(nameof(IRandomGenerator));
            _gameConfig = _gameConfigurator.GetConfig();

        }

        public void NewRandomGame()
        {
            switch (MakeMainMenu())
            {
                case 1:
                    RunGame();
                    return;
                case 2:
                    NewConfig();
                    return;
                default:
                    break;
            }
        }

        private int MakeMainMenu()
        {
            Console.WriteLine("Игра 'Угадай число'");
            Console.WriteLine("Для запуска нажмите 1");
            Console.WriteLine("Для настройки параметров нажмите 2");
            Console.WriteLine("Для выхода нажмите 0");
            while (true)
            {
                var key = Console.ReadKey().KeyChar;
                switch (key)
                {
                    case '1':
                        return 1;
                    case '2':
                        return 2;
                    case '0':
                        return 0;
                    default:
                        Console.WriteLine("Неверный параметр. Повторите ввод");
                        break;
                }
                Console.Clear();
            }
        }

        private void RunGame()
        {
            int rndVal = _randomGenerator.GenerateNumberInRange(_gameConfig.MinValue, _gameConfig.MaxValue);
            int attemptsCount = _gameConfig.AttemptsCount;
            bool win = false;
            string s = string.Empty;
            while (attemptsCount > 0)
            {
                Console.WriteLine("");
                Console.WriteLine($" Ввведите число. Осталось попыток {attemptsCount}");
                int usrVal = 0;
                var tryParserRes = int.TryParse(Console.ReadLine(), out usrVal);
                win = Check(usrVal, rndVal);
                if (tryParserRes && win)
                {
                    Console.WriteLine("Вы угадали!");
                    return;
                }
                Console.WriteLine("Не угадали ");
                Console.Write( usrVal>rndVal? "Загаданное число меньше" :  "Загаданное число больше");
                attemptsCount--;
            }
            Console.WriteLine($"Вы не смогли угадать за заданное количество попыток! Было загадано число {rndVal}");
        }
        
        private bool Check(int userValue, int rndValue) => userValue == rndValue;

        private void NewConfig()
        {
            Console.WriteLine("Настройка параметров игры:");
            GameConfigurationType newConfig = new GameConfigurationType();
            Console.WriteLine("Введите количество попыток:");
            var res = int.TryParse(Console.ReadLine(), out var param);
            if (!res)
            {
                Console.WriteLine("Ошибка ввода параметра. Повторите заново.");
                return;
            }
            newConfig.AttemptsCount = param;
            
            Console.WriteLine("Введите минимальное загадываемое число:");
            res = int.TryParse(Console.ReadLine(), out param);
            if (!res)
            {
                Console.WriteLine("Ошибка ввода параметра. Повторите заново.");
                return;
            }
            newConfig.MinValue = param;
            
            Console.WriteLine("Введите максимальное загадываемое число:");
            res = int.TryParse(Console.ReadLine(), out param);
            if (!res)
            {
                Console.WriteLine("Ошибка ввода параметра. Повторите заново.");
                return;
            }
            newConfig.MaxValue = param;

            if (!_gameConfigurator.ValidateConfig(newConfig))
            {
                Console.WriteLine("Ошибка валидации параметров. Повторите заново.");
                return;
            };
            if (!_gameConfigurator.SetConfig(newConfig))
            {
                Console.WriteLine("Ошибка сохранения конфигурации. Повторите заново");
                return;
            }
            Console.WriteLine("Конфигурация сохранена.");
        }
        
    }
}