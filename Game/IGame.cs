using System.Threading.Tasks;

namespace Game
{
    public interface IGame
    {
        ///<summary> Запуск игры <summary>
        Task RunGameAsync();
    }
}