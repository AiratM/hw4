namespace Game.Configurator
{
    public interface IConfiguratorWriter
    {
        bool WriteConfig(GameConfigurationType newConfig);
    }
}