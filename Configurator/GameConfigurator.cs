using System;

namespace Game.Configurator
{
    // Liskov Substitution Principle: мы пользуемся реализацией IConfiguratorReader,
    // IConfiguratorWriter IConfigurationValidator не завися от их реализаций
    public class GameConfigurator : IConfigurator
    {
        private IConfigurationValidator _configValidator;
        private IConfiguratorReader _configReader;
        private IConfiguratorWriter _configWriter;

        public GameConfigurator(IConfigurationValidator configurationValidator, IConfiguratorReader configuratorReader,
                            IConfiguratorWriter configuratorWriter)
        {
            _configValidator = configurationValidator ?? throw new NullReferenceException(nameof(IConfigurationValidator));
            _configReader = configuratorReader ?? throw new NullReferenceException(nameof(IConfiguratorReader));
            _configWriter = configuratorWriter ?? throw new NullReferenceException(nameof(IConfiguratorWriter));

        }
        public GameConfigurationType GetConfig()
        {
            GameConfigurationType? gameConfiguration = _configReader.ReadConfig();
            if ((gameConfiguration is not null) && ValidateConfig(gameConfiguration))
                return gameConfiguration;

            var cfg =  new GameConfigurationType
            {
                MinValue = 0,
                MaxValue = 100,
                AttemptsCount = 5
            };
            SetConfig(cfg);
            return cfg;
        }

        public bool SetConfig(GameConfigurationType newConfig)
        {
            if (!ValidateConfig(newConfig))
                return false;
            return _configWriter.WriteConfig(newConfig);
        }
        /// <inheritdoc />
        public bool ValidateConfig(GameConfigurationType newConfig)
        {
            return _configValidator.ValidateAttemptsCount(newConfig.AttemptsCount) &&
            _configValidator.ValidateMaxValue(newConfig.MaxValue) &&
            _configValidator.ValidateMinValue(newConfig.MinValue);
        }
    }
}