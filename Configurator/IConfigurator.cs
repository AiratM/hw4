namespace Game.Configurator
{
    // Single Responsibility Principle

    // Interface Segregation Principle
    public interface IConfigurator
    {
        ///<summary> Чтение конфигурации<summary>
        GameConfigurationType GetConfig();

        ///<summary> Сохранение конфигурации <summary>
        bool SetConfig(GameConfigurationType newConfig);

        ///<summary> Проверка конфигурации <summary>
        bool ValidateConfig(GameConfigurationType newConfig);
    }
}