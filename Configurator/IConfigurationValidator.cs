namespace Game.Configurator {
    public interface IConfigurationValidator {

        ///<summary> Проверка параметра "Минимальное значение генерируемого диапазона" <summary>
        bool ValidateMinValue(int minValue);

        ///<summary> Проверка параметра "Максимальное значение генерируемого диапазона" <summary>
        bool ValidateMaxValue(int maxValue);
        
        ///<summary> Проверка параметра "Количество попыток отгадываний" <summary>
        bool ValidateAttemptsCount(int attemptsCount);
    }
}