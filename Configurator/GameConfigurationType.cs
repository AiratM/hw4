namespace Game.Configurator
{
    public class GameConfigurationType
    {
        ///<summary> Минимальное значение генерируемого диапазона <summary>
        public int MinValue { get; set; }

        ///<summary> Максимальное значение генерируемого диапазона <summary>
        public int MaxValue { get; set; }
        
        ///<summary> Количество попыток отгадываний <summary>
        public int AttemptsCount { get; set; }
    }
}