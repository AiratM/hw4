using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Game.Configurator
{
    public class ConfiguratorXMLFileReader : IConfiguratorReader
    {
        public GameConfigurationType? ReadConfig()
        {
            if (File.Exists("config.xml"))
            {
                using (var reader = new StreamReader("config.xml", false))
                {
                    var serializer = new XmlSerializer(typeof(GameConfigurationType));
                    return (GameConfigurationType?)serializer.Deserialize(reader);
                }
            }
            return null;
        }
    }
}