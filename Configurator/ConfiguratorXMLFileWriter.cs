using System.IO;
using System.Xml.Serialization;

namespace Game.Configurator
{
    public class ConfiguratorXMLFileWriter : IConfiguratorWriter
    {
        public bool WriteConfig(GameConfigurationType newConfig)
        {
            using (var writer = new StreamWriter("config.xml", false))
            {
                var serializer = new XmlSerializer(typeof(GameConfigurationType));
                serializer.Serialize(writer, newConfig);
            };
            return true;
        }
    }
}