namespace Game.Configurator {

    public interface IConfiguratorReader 
    {
        GameConfigurationType? ReadConfig();
    }
}