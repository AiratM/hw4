namespace Game.Configurator
{
    public class ConfigurationValidator : IConfigurationValidator
    {
        public bool ValidateMinValue(int minValue)
        {
            return minValue >= 0;
        }

        public bool ValidateMaxValue(int maxValue)
        {
            return maxValue <= 100;
        }

        public bool ValidateAttemptsCount(int attemptsCount)
        {
            return attemptsCount <= 10;
        }
    }
}